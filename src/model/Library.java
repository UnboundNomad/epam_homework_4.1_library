package model;

public class Library {

    public final int SIZE;
    private Book[] books;

    public Library(int SIZE) {
        this.SIZE = SIZE;
        this.books = new Book[SIZE];
    }

    public Book getBook(int index) {
        return books[index];
    }

    public void setBook(int index, Book book) {
        this.books[index] = book;
    }

}

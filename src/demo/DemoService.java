package demo;

import controllers.ILibrary;
import model.Book;
import model.Library;

public class DemoService {

    private Library library;
    private ILibrary service;

    public DemoService(Library library, ILibrary service) {
        this.library = library;
        this.service = service;
    }

    public void execute () {

        System.out.println("Библиотка с максимальным размером 8 книг");
        fill();

        showAllBooks();

        //Добавление
        System.out.println("Добавим ещё две книги:");
        Book book1 = new Book("Мёртвые души", "Н. В. Гоголь");
        Book book2 = new Book("Сумма биотехнологии", "Александр Панчин");

        System.out.println(book1.toString());
        System.out.println(book2.toString());
        System.out.println();

        tryAddBook(book1);
        tryAddBook(book2);

        showAllBooks();

        System.out.println("Попробуем добавить ещё одну:");
        book1 = new Book("Core Java", "Cay Horstmann");
        System.out.println(book1.toString());
        System.out.println();
        tryAddBook(book1);

        //Поиск
        String search = "harry and";
        System.out.printf("\nНайдём книги по запросу \"%s\":\n", search);

        Book[] books = service.findBooks(library, search);

        for (Book book : books){
            System.out.println(book.toString());
        }

        //Удаление
        System.out.println("\nИ удалим первую из них:");
        tryDeleteBook(books[0]);
        showAllBooks();

        book1 = new Book ("On the Origin of Species", "Charles Darwin");
        System.out.println("Попробуем удалить книгу: ");
        System.out.println(book1.toString());
        System.out.println();
        tryDeleteBook(book1);

        //Ещё одно добавление
        System.out.println("\nТогда добавим её туда:");
        tryAddBook(book1);

        showAllBooks();

    }

    private void showAllBooks() {

        System.out.printf("\n%50s\n","Книги в библиотеке:");
        System.out.print("№ ");
        showTitle();

        Book[] books = service.findBooks(library, "");

        int i = 1;

        for (Book book : books) {
            System.out.printf("%-3d", i++);
            System.out.println(book);
        }
        System.out.println();
    }

    private void tryAddBook(Book book) {
        if (service.addBook(library, book)) {
            System.out.printf("Книга \"%s\" была добавлена\n", book.getTitle());
        } else {
            System.out.println("Библиотека переполнена или книга повреждена");
        }
    }

    private void tryDeleteBook(Book book) {
        if (service.deleteBook(library, book)) {
            System.out.printf("Книга \"%s\" была удалена\n", book.getTitle());
        } else {
            System.out.println("Указанной книги нет в библиотеке");
        }
    }

    private void fill() {
        service.addBook(library, new Book("The Hobbit, or There and Back Again", "J. R. R. Tolkien"));
        service.addBook(library, new Book("Nineteen Eighty-Four (1984)", "George Orwell"));
        service.addBook(library, new Book("Harry Potter and the Philosopher's Stone", "J. K. Rowling"));
        service.addBook(library, new Book("Сказка о Тройке", "Аркадий и Борис Стругацкие"));
        service.addBook(library, new Book("Хитроумный идальго Дон Кихот Ламанчский", "Мигель де Сервантес"));
        service.addBook(library, new Book("Harry Potter and the Methods of Rationality", "Eliezer Yudkowsky"));
    }

    private void showTitle() {
        System.out.printf("%-50s %s\n", "Название:", "Автор:");
    }
}

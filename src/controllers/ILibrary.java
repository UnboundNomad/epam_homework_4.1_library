package controllers;

import model.Book;
import model.Library;

public interface ILibrary {

    boolean addBook(Library library, Book book);

    boolean deleteBook(Library library, Book book);

    Book[] findBooks (Library library, String search);
}

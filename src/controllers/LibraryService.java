package controllers;

import model.Book;
import model.Library;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class LibraryService implements ILibrary {

    @Override
    public boolean addBook(final Library library, final Book book) {

        if (book == null) {
            return false;
        }

        int nullIndex = findBookIndex(library, null);

        if (nullIndex == -1) {
            return false;
        }

        library.setBook(nullIndex, book);
        return true;
    }

    @Override
    public boolean deleteBook(final Library library, final Book book) {

        int index = findBookIndex(library, book);

        if (index == -1) {
            return false;
        }

        library.setBook(index, null);
        return true;
    }

    @Override
    public Book[] findBooks(final Library library, final String search) {

        List<Book> books = new ArrayList<>();
        String[] searchArray = search.split(" ");

        for (int i = 0; i < library.SIZE; i++) {

            Book book = library.getBook(i);

            if (book == null) {
                continue;
            }

            String bookName = String.format("%s %s", book.getTitle(), book.getAuthor());
            if (isWordsInString(searchArray, bookName)) {
                books.add(book);
            }
        }

        return books.toArray(new Book[0]);
    }


    private int findBookIndex(Library library, Book book) {

        for (int i = 0; i < library.SIZE; i++) {
            if (library.getBook(i) == book) {
                return i;
            }
        }
        return -1;
    }

    private boolean isWordsInString(String[] words, String string) {

        for (String word : words) {
            if (!Pattern.compile(String.format("(?i)%s", word.trim())).matcher(string).find()) {
                return false;
            }
        }
        return true;
    }
}

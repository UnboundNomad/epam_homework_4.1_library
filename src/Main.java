import controllers.LibraryService;
import demo.DemoService;
import model.Library;

public class Main {

    public static void main(String[] args) {

        new DemoService(new Library(8), new LibraryService()).execute();

    }

}
